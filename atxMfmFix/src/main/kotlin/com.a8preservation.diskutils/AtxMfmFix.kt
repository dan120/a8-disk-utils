/**
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.a8preservation.diskutils

import com.a8preservation.disklib.*
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.SystemExitException
import java.io.File
import java.util.*

class DiskCompareArgs(parser: ArgParser, strings: ResourceBundle) {
    val sfile by parser.positional(strings.getString("sFileDesc"))
    val dfile by parser.positional(strings.getString("dFileDesc"))
}

fun printDiskCompareHeader(props: Properties, strings: ResourceBundle) {
    println("${strings.getString("programName")} v${props.get("version")}")
    println(strings.getString("programCopyright"))
    println(strings.getString("programLicense"))
    println(strings.getString("programHint"))
    println()
}

fun convertAtxToMfm(sfile: File, dfile: File) {
    val disk = readATX(sfile.inputStream())

    val newTracks = ArrayList<Track>()
    for (track in disk.tracks) {
        newTracks.add(Track(track.number, TrackEncoding.MFM, track.sectors))
    }

    val disk2 = Disk(DiskDensity.MEDIUM, newTracks)

    writeATX(dfile, disk2)
}

/**
 * A utility that creates a copy of an ATX file with the appropriate MFM flags set.
 */
fun main(cargs : Array<String>) {
    val strings = ResourceBundle.getBundle("strings")

    val props = Properties()
    props.load(ClassLoader.getSystemResource("version.properties").openStream())

    try {
        val args = DiskCompareArgs(ArgParser(cargs), strings)
        printDiskCompareHeader(props, strings)
        convertAtxToMfm(File(args.sfile), File(args.dfile))
    } catch (e: SystemExitException) {
        e.printAndExit("atxMfmFix", 80)
    }
}
