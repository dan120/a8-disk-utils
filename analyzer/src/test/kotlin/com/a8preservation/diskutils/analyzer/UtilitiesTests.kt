package com.a8preservation.diskutils.analyzer

import calculateNextSector
import com.a8preservation.disklib.DiskDensity
import com.a8preservation.disklib.createCRC32BString
import com.a8preservation.disklib.readATX
import com.fasterxml.jackson.databind.ObjectMapper
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.fail
import org.junit.Test
import parseSectorPositionString
import readJSON
import writeDiskToJSON
import java.io.File
import java.util.zip.CRC32

class UtilitiesTests {
    @Test
    fun testParseSectorPositionString1() {
        val (num,pos) = parseSectorPositionString("18")
        assertEquals(18, num)
        assertEquals(-1, pos)
    }

    @Test
    fun testParseSectorPositionString2() {
        val (num,pos) = parseSectorPositionString("10.100")
        assertEquals(10, num)
        assertEquals(100, pos)
    }

    @Test
    fun testParseSectorPositionString3() {
        try {
            val (num,pos) = parseSectorPositionString("ABC")
            fail("Should have thrown an exception")
        } catch (e: Exception) {}
    }

    @Test
    fun testReadJSON() {
        convertATXToJsonAndTest(File("./src/test/resources/disk1.atx"))
    }

    @Test
    fun testReadJSONWithWeakData() {
        convertATXToJsonAndTest(File("./src/test/resources/disk2.atx"))
    }

    @Test
    fun testCreateCRC32String() {
        val crc32 = CRC32()
        crc32.update(100)
        crc32.update(200)
        crc32.update(300)
        assertEquals("81648b4b", createCRC32BString(crc32))
    }

    @Test
    fun testCalculateNextSector() {
        assertEquals(127u, calculateNextSector(0x10u,0x7Fu))
        assertEquals(128u, calculateNextSector(0x10u,0x80u))
        assertEquals(129u, calculateNextSector(0x10u,0x81u))
        assertEquals(255u, calculateNextSector(0x10u,0xFFu))
        assertEquals(256u, calculateNextSector(0x11u,0x00u))
    }

    fun convertATXToJsonAndTest(file: File) {
        // read ATX file
        val diskAtx = readATX(file.inputStream())

        val tmpFile = File.createTempFile("test-", ".json")
        tmpFile.deleteOnExit()

        // create JSON file from ATX disk
        writeDiskToJSON(tmpFile, ObjectMapper(), diskAtx)

        // read JSON file
        val disk = readJSON(tmpFile, ObjectMapper())

        // validate disk object
        assertEquals(DiskDensity.SINGLE, disk.density)
        assertEquals(40, disk.trackCount)
        for (ti in 0..39) {
            val told = diskAtx.tracks[ti]
            val tnew = disk.tracks[ti]
            assertEquals(told.number, tnew.number)
            for (snew in tnew.sectors) {
                val sold = told.getFirstSector(snew.number)
                assertEquals(sold!!.fdcStatus, snew.fdcStatus)
                assertEquals(sold.position, snew.position)
                assertEquals(sold.weakOffset, snew.weakOffset)
                assertEquals(sold.data!!.size, snew.data!!.size)
            }
        }
    }
}
