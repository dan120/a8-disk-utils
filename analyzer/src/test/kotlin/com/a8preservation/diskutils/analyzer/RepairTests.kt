package com.a8preservation.diskutils.analyzer

import com.a8preservation.disklib.*
import junit.framework.Assert.assertNull
import junit.framework.TestCase.assertEquals
import org.junit.Test

class RepairTests {
    @Test
    fun testOneSectorWithOneGood() {
        val srp = SectorRepairPlan(1)
        srp.addSector(Sector(1, byteArrayOf(1), 0, 0))
        assertEquals(1, srp.getRepairData()!![0])
    }

    @Test
    fun testMixedSectorsWithOneGood() {
        val srp = SectorRepairPlan(1)
        srp.addSector(Sector(1, byteArrayOf(1), 0, 8))
        srp.addSector(Sector(1, byteArrayOf(2), 0, 0))
        srp.addSector(Sector(1, byteArrayOf(3), 0, 8))
        assertEquals(2, srp.getRepairData()!![0])
        assertEquals(0, srp.getRepairedFdcStatus())
        assertNull(srp.getRepairedWeakOffset())
    }

    @Test
    fun testMixedSectorsWithNoGood() {
        val srp = SectorRepairPlan(1)
        srp.addSector(Sector(1, byteArrayOf(1), 0, 8))
        srp.addSector(Sector(1, byteArrayOf(2), 0, 8))
        srp.addSector(Sector(1, byteArrayOf(3), 0, 8))
        assertEquals(3, srp.getRepairData()!![0])
        assertEquals(8, srp.getRepairedFdcStatus())
        assertNull(srp.getRepairedWeakOffset())
    }

    @Test
    fun testCreateRepairPlanWithMissingSectorAndGoodReplacement() {
        val disk1 = Disk(DiskDensity.SINGLE, listOf(
            Track(0, TrackEncoding.FM, listOf(
                Sector(1, byteArrayOf(5), 0, 0),
                Sector(2, byteArrayOf(4), 0, 0)
            ))
        ))
        val disk2 = Disk(DiskDensity.SINGLE, listOf(
                Track(0, TrackEncoding.FM, listOf(
                        Sector(1, byteArrayOf(1), 0, 0),
                        Sector(2, byteArrayOf(2), 0, 0),
                        Sector(3, byteArrayOf(3), 0, 0)
                ))
        ))
        val rp = createRepairPlan(disk1, listOf(disk2))
        assertEquals(1, rp.sectorRepairPlans.size)
        assertEquals(3, rp.sectorRepairPlans[0].number)
    }

    @Test
    fun testCreateRepairPlanWithMissingSectorAndBadReplacement() {
        val disk1 = Disk(DiskDensity.SINGLE, listOf(
                Track(0, TrackEncoding.FM, listOf(
                        Sector(1, byteArrayOf(5), 0, 0),
                        Sector(2, byteArrayOf(4), 0, 0)
                ))
        ))
        val disk2 = Disk(DiskDensity.SINGLE, listOf(
                Track(0, TrackEncoding.FM, listOf(
                        Sector(1, byteArrayOf(1), 0, 0),
                        Sector(2, byteArrayOf(2), 0, 0),
                        Sector(3, byteArrayOf(3), 0, 8)
                ))
        ))
        val rp = createRepairPlan(disk1, listOf(disk2))
        assertEquals(1, rp.sectorRepairPlans.size)
        assertEquals(3, rp.sectorRepairPlans[0].number)
    }

    @Test
    fun testCreateRepairPlanWithBadSectorAndBadReplacements() {
        val disk1 = Disk(DiskDensity.SINGLE, listOf(
                Track(0, TrackEncoding.FM, listOf(
                        Sector(1, byteArrayOf(1), 0, 0),
                        Sector(2, byteArrayOf(2), 0, 0),
                        Sector(3, byteArrayOf(3), 0, 8)
                ))
        ))
        val disk2 = Disk(DiskDensity.SINGLE, listOf(
                Track(0, TrackEncoding.FM, listOf(
                        Sector(1, byteArrayOf(4), 0, 0),
                        Sector(2, byteArrayOf(5), 0, 0),
                        Sector(3, byteArrayOf(6), 0, 8)
                ))
        ))
        val rp = createRepairPlan(disk1, listOf(disk2))
        assertEquals(0, rp.sectorRepairPlans.size)
    }

    @Test
    fun testApplyRepairPlanWithSimpleReplacement() {
        val tracks = listOf(Track(0, TrackEncoding.FM, listOf(
                Sector(1, byteArrayOf(1), 0, 0),
                Sector(2, byteArrayOf(2), 100, 8)
        )))
        val disk = Disk(DiskDensity.SINGLE, tracks)
        val repairPlan = DiskRepairPlan().addSectorRepairPlan(SectorRepairPlan(2).addSector(Sector(2, byteArrayOf(3), 101, 0)))
        applyRepairPlan(disk, repairPlan)
        assertEquals(1, disk.trackCount)
        assertEquals(2, disk.tracks[0].sectorCount)
        assertEquals(1, disk.getSector(1).size)
        assertEquals(1, disk.getSector(1)[0].data!![0])
        assertEquals(0, disk.getSector(1)[0].fdcStatus)
        assertEquals(1, disk.getSector(2).size)
        assertEquals(3, disk.getSector(2)[0].data!![0])
        assertEquals(0, disk.getSector(2)[0].fdcStatus)
    }

    @Test
    fun testApplyRepairPlanWithMissingSectorReplacement() {
        val tracks = listOf(Track(0, TrackEncoding.FM, listOf(
                Sector(1, byteArrayOf(1), 0, 0),
                Sector(2, byteArrayOf(2), 100, 0)
        )))
        val disk = Disk(DiskDensity.SINGLE, tracks)
        val repairPlan = DiskRepairPlan().addSectorRepairPlan(SectorRepairPlan(3).addSector(Sector(3, byteArrayOf(3), 150, 8)))
        applyRepairPlan(disk, repairPlan)
        assertEquals(1, disk.trackCount)
        assertEquals(3, disk.tracks[0].sectorCount)
        assertEquals(1, disk.getSector(1).size)
        assertEquals(1, disk.getSector(1)[0].data!![0])
        assertEquals(0, disk.getSector(1)[0].fdcStatus)
        assertEquals(1, disk.getSector(2).size)
        assertEquals(2, disk.getSector(2)[0].data!![0])
        assertEquals(0, disk.getSector(2)[0].fdcStatus)
        assertEquals(1, disk.getSector(3).size)
        assertEquals(3, disk.getSector(3)[0].data!![0])
        assertEquals(8, disk.getSector(3)[0].fdcStatus)
    }

    @Test
    fun testSectorRepairPlanWithIdenticalData() {
        val repairPlan = SectorRepairPlan(3)
                .addSector(Sector(3, byteArrayOf(1,2,3), 150, 0))
                .addSector(Sector(3, byteArrayOf(1,2,3), 150, 0))
                .addSector(Sector(3, byteArrayOf(1,2,3), 150, 0))
        assertEquals(1, repairPlan.getRepairData()!![0])
        assertEquals(2, repairPlan.getRepairData()!![1])
        assertEquals(3, repairPlan.getRepairData()!![2])
    }

    @Test
    fun testSectorRepairPlanWithVaryingBadDataAndOneGood() {
        val repairPlan = SectorRepairPlan(3)
                .addSector(Sector(3, byteArrayOf(4,5,6), 150, 8))
                .addSector(Sector(3, byteArrayOf(1,2,3), 150, 0))
                .addSector(Sector(3, byteArrayOf(4,5,6), 150, 8))
        assertEquals(1, repairPlan.getRepairData()!![0])
        assertEquals(2, repairPlan.getRepairData()!![1])
        assertEquals(3, repairPlan.getRepairData()!![2])
    }

    @Test
    fun testSectorRepairPlanWithVaryingBadData() {
        val repairPlan = SectorRepairPlan(3)
                .addSector(Sector(3, byteArrayOf(1,2,5), 150, 8))
                .addSector(Sector(3, byteArrayOf(1,6,3), 150, 8))
                .addSector(Sector(3, byteArrayOf(7,2,3), 150, 8))
        assertEquals(1, repairPlan.getRepairData()!![0])
        assertEquals(2, repairPlan.getRepairData()!![1])
        assertEquals(3, repairPlan.getRepairData()!![2])
    }
}