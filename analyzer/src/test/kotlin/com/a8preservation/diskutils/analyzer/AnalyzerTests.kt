/**
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.a8preservation.diskutils.analyzer

import convertSectorPositionToIndex
import junit.framework.TestCase.assertEquals
import org.junit.Test

class AnalyzerTests {
    @Test
    fun testConvertSectorPositionToIndex() {
        assertEquals(0, convertSectorPositionToIndex(0))
        assertEquals(0, convertSectorPositionToIndex(250))
        assertEquals(1, convertSectorPositionToIndex(251))
        assertEquals(1, convertSectorPositionToIndex(500))
        assertEquals(2, convertSectorPositionToIndex(501))
        assertEquals(2, convertSectorPositionToIndex(750))
        assertEquals(3, convertSectorPositionToIndex(751))
        assertEquals(103, convertSectorPositionToIndex(26000))
        assertEquals(104, convertSectorPositionToIndex(26001))
        assertEquals(104, convertSectorPositionToIndex(26042))
    }
}
