package com.a8preservation.diskutils.analyzer

import com.a8preservation.disklib.*
import java.util.*

fun createRepairPlan(disk: Disk, repairDisks: Collection<Disk>): DiskRepairPlan {
    val sectorsPerTrack = disk.sectorsPerTrack
    val maxSectorCount = sectorsPerTrack * disk.trackCount
    val diskRepairPlan = DiskRepairPlan()

    for (i in 1..maxSectorCount) {
        val sectorList = disk.getSector(i)
        if (sectorList.size <= 1) {
            val sector = sectorList.firstOrNull()
            if (sector == null || sector.hasError) {
                val sectorRepairPlan = SectorRepairPlan(i)
                for (repairDisk in repairDisks) {
                    val repairSectorList = repairDisk.getSector(i)
                    if (repairSectorList.size == 1 && (sector == null || !repairSectorList[0].hasError)) {
                        sectorRepairPlan.addSector(repairSectorList[0])
                    }
                }
                if (sectorRepairPlan.hasRepairSectors()) {
                    diskRepairPlan.addSectorRepairPlan(sectorRepairPlan)
                }
            }
        }
    }
    return diskRepairPlan
}

fun applyRepairPlan(disk: Disk, repairPlan: DiskRepairPlan, log: StringBuilder? = null) {
    for (srp in repairPlan.sectorRepairPlans) {
        val sectorList = disk.getSector(srp.number)
        if (sectorList.size == 1) {
            sectorList[0].data = srp.getRepairData()
            sectorList[0].fdcStatus = srp.getRepairedFdcStatus()
            sectorList[0].weakOffset = srp.getRepairedWeakOffset()
            log?.append("S${String.format("%03d", srp.number)} replaced data in existing sector\n")
        } else if (sectorList.size == 0) {
            val track = disk.getTrack(getTrackNumberForAbsoluteSectorNumber(srp.number, disk.sectorsPerTrack))
            val newSector = Sector(getTrackSectorNumberForAbsoluteSectorNumber(srp.number, disk.sectorsPerTrack))
            newSector.data = srp.getRepairData()
            newSector.fdcStatus = srp.getRepairedFdcStatus()
            newSector.weakOffset = srp.getRepairedWeakOffset()
            newSector.position = 0
            track.sectors = track.sectors.plus(newSector)
            log?.append("S${String.format("%03d", srp.number)} re-added missing sector\n")
        }
    }
}

class DiskRepairPlan {
    val sectorRepairPlans = ArrayList<SectorRepairPlan>()

    fun addSectorRepairPlan(repairPlan: SectorRepairPlan): DiskRepairPlan {
        sectorRepairPlans.add(repairPlan)
        return this
    }
}

class SectorRepairPlan(val number: Int) {
    val repairSectors = ArrayList<Sector>()

    fun addSector(sector: Sector): SectorRepairPlan {
        repairSectors.add(sector)
        return this
    }

    fun hasRepairSectors(): Boolean {
        return repairSectors.size > 0
    }

    fun isFullyRepairable(): Boolean {
        return (repairSectors.find { !it.hasError }) != null
    }

    fun getRepairData(): ByteArray? {
        var sector = repairSectors.find { !it.hasError }
        return if (sector != null) sector.data else {
            // find first sector with data so we can determine sector size
            // note: this assumes all sectors are the same size
            sector = repairSectors.find { it.hasData }
            if (sector != null) {
                val newData = ByteArray(sector.data!!.size)
                for (i in newData.indices) {
                    // build a frequency map for each byte
                    val fmap = sortedMapOf<Byte,Int>()
                    for (rs in repairSectors) {
                        if (rs.hasData) {
                            val count = fmap.get(rs.data!![i])
                            fmap.put(rs.data!![i], if (count != null) count+1 else 1)
                        }
                    }
                    // sort my frequency and choose the most frequent (or last read if they are all the same frequency)
                    val sfmap = fmap.toList().sortedBy { (_, value) -> value }.toMap()
                    newData[i] = sfmap.keys.last()
                }
                newData
            } else {
                // if no sectors have data, just return the first repair sector's data
                repairSectors.first().data
            }
        }
    }

    fun getRepairedFdcStatus(): Int {
        val sector = repairSectors.find { !it.hasError }
        return if (sector != null) sector.fdcStatus else repairSectors.first().fdcStatus
    }

    fun getRepairedWeakOffset(): Int? {
        val sector = repairSectors.find { !it.hasError }
        return if (sector != null) sector.weakOffset else repairSectors.first().weakOffset
    }
}