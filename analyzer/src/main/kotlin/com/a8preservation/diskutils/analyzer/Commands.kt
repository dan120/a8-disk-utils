package com.a8preservation.diskutils.analyzer

import java.io.File
import java.util.*
import javax.annotation.PostConstruct

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.hash.Hashing
import com.google.common.io.Files
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ansi.AnsiColor
import org.springframework.boot.ansi.AnsiOutput
import org.springframework.shell.standard.ShellOption

import com.a8preservation.disklib.*
import alterAbsoluteSector
import calculateNextSector
import convertSectorPositionToIndex
import createCRC32StringFromSectors
import createSectorString
import detectSectorDescription
import org.springframework.shell.Availability
import org.springframework.shell.standard.ShellMethodAvailability
import parseSectorPositionString
import readDiskImage
import writeDiskToJSON
import writeSectorsToJSON
import java.util.zip.CRC32

@ShellComponent
class Commands {
    @Autowired
    val arguments: ApplicationArguments? = null
    @Autowired
    val objectMapper: ObjectMapper? = null

    var loadFile: File? = null
    var loadedDisk: Disk? = null
    var repairDisks: ArrayList<Disk> = ArrayList<Disk>()
    val strings = ResourceBundle.getBundle("strings")

    @PostConstruct
    fun startup() {
        if (arguments != null && arguments!!.sourceArgs.isNotEmpty()) {
            val fn = arguments?.sourceArgs?.get(0)
            if (fn != null && !fn.startsWith("@")) {
                println(load(File(fn)))
            }
        }
    }

    @ShellMethod(value = "Set color output", group = "")
    fun color(@ShellOption(help = "on or off")value: String): String {
        if ("on".equals(value.toLowerCase())) {
            AnsiOutput.setEnabled(AnsiOutput.Enabled.ALWAYS);
        } else if ("off".equals(value.toLowerCase())) {
            AnsiOutput.setEnabled(AnsiOutput.Enabled.NEVER);
        }
        return "";
    }

    @ShellMethod(value = "Load disk image file", group = "Disk")
    fun load(@ShellOption file: File): String {
        // make sure the files exist
        if (!file.isFile) {
            return "${file.absolutePath} ${strings.getString("notFoundSuffix")}"
        }

        // read the disk image
        loadFile = file
        loadedDisk = readDiskImage(file, objectMapper!!)

        return if (loadedDisk != null) {
            // scan through for any recognized sectors
            for (track in loadedDisk!!.tracks) {
                for (sector in track.sectors) {
                    if (sector.hasData) {
                        sector.description = detectSectorDescription(String.format("%08x", sector.dataCrcMinus3.value))
                    }
                }
            }

            repairDisks.clear()

            StringBuilder("Successfully loaded image file.\n\n").append(summary()).toString()
        } else {
            "Unable to load disk"
        }
    }

    @ShellMethod(value = "Replace sector data/fdcStatus on current disk with sector from another image file", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun copySectorData(@ShellOption(help = "Source file path")srcFile: File, @ShellOption(help = "Source track number (0-39)")srcTrackNum: Int, @ShellOption(help = "Source sector position")srcSectorPos: Int, @ShellOption(help = "Destination track number (0-39)")dstTrackNum: Int, @ShellOption(help = "Destination sector position")dstSectorPos: Int): String {
        if (!srcFile.isFile) {
            return "${srcFile.absolutePath} ${strings.getString("notFoundSuffix")}"
        }
        val disk = readDiskImage(srcFile, objectMapper!!)
        when (val srcSector = disk.getTrack(srcTrackNum).getSectorByPosition(srcSectorPos)) {
            null -> {
                return "Source sector not found in specified disk image"
            }
            else -> {
                val dstTrack = loadedDisk!!.getTrack(dstTrackNum)
                when (val dstSector = dstTrack.getSectorByPosition(dstSectorPos)) {
                    null -> {
                        return "Destination sector not found"
                    }
                    else -> {
                        dstTrack.replaceSectorData(dstSector.index, srcSector.value)
                    }
                }
            }
        }
        return "Done"
    }

    @ShellMethod(value = "Show protection summary string", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun prot(): String {
        return createProtectionSummary(loadedDisk!!)
    }

    @ShellMethod(value = "Show disk image summary", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun summary(): String {
        // create table model
        val builder = StringBuilder()
        builder.append("File:        ${loadFile!!.name}\n")
        builder.append("Size:        ${loadFile!!.length()}\n")
        builder.append("CRC:         ${createCRC32BString(loadFile!!)}\n")
        builder.append("MD5:         ${Files.hash(loadFile!!, Hashing.md5())}\n")
        builder.append("SHA1:        ${Files.hash(loadFile!!, Hashing.sha1())}\n")
        builder.append("Density:     ${loadedDisk?.density.toString().toLowerCase().capitalize()}\n")
        builder.append("Track count: ${loadedDisk?.tracks?.size.toString()}\n")
        builder.append("Sector size: ${loadedDisk?.sectorSize?.toString() ?: "Unknown"}\n")
        builder.append("VTOC avail:  ${loadedDisk?.getVTOC()?.availableSectorCount ?: "Unknown"}\n")
        builder.append("VTOC unused: ${loadedDisk?.getVTOC()?.unusedSectorCount ?: "Unknown"}\n")

        // render table
        return builder.toString()
    }

    @ShellMethod(value = "Show disk file directory", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun dir(): String {
        return try {
            val dir = loadedDisk!!.getFileDirectory()
            if (dir.isNotEmpty()) {
                val builder = StringBuilder()
                for (f in dir) {
                    val crc = createCRC32StringFromSectors(loadedDisk!!, f.startSector, f.sectorCount)
                    builder.append("${if (f.isLocked) '*' else ' '}${f.filename}.${f.extension} - start:${String.format("%03d", f.startSector)}, length:${String.format("%03d", f.sectorCount)}, crc32:$crc\n")
                }
                builder.toString()
            } else {
                "No files found"
            }
        } catch (e: RuntimeException) {
            e.localizedMessage
        }
    }

    @ShellMethod(value = "Show track summary", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun trackSummary(@ShellOption(help = "Track number (0-39)")number: Int): String {
        val builder = StringBuilder()
        val track = loadedDisk!!.getTrack(number)

        builder.append("Drive RPM:  ${track.driveRpm}\n")
        builder.append("Flux count: ${track.fluxCount}\n")

        return builder.toString()
    }

    @ShellMethod(value = "Show position delta between sector and previous sector for track", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun sectorPositionDeltas(@ShellOption(help = "Track number (0-39)")number: Int): String {
        val track = loadedDisk!!.getTrack(number)
        val builder = StringBuilder()
        for ((i, s) in track.sectors.withIndex()) {
            var prevIndex = i-1
            if (prevIndex > 0) {
                var s2 = track.sectors[prevIndex]
                builder.append("${s.number}: ${s.position} (${s.position - s2.position})\n")
            }
        }
        return builder.toString()
    }

    @ShellMethod(value = "Insert empty sector into track", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun insertEmptySector(@ShellOption(help = "Track number (0-39)")tnum: Int, @ShellOption(help = "Sector number (1-18)")snum: Int, @ShellOption(help = "Angular position on track")pos: Int): String {
        val track = loadedDisk!!.getTrack(tnum)
        track.insertEmptySector(snum, pos)
        return "Done"
    }

    @ShellMethod(value = "Show flux transition durations (in microseconds)", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun flux(@ShellOption(help = "Track number (0-39)")number: Int, @ShellOption(help = "Index of flux (0-fluxCount)")index: Int): String {
        val track = loadedDisk!!.getTrack(number)
        val fluxData = track.flux?.get(index)
        var time = 0.0

        if (fluxData != null) {
            val builder = StringBuilder()
            for ((i, v) in fluxData.rawData.withIndex()) {
                builder.append(String.format("%05.2f", v))
                if (i != 0 && i % 17 == 0) {
                    builder.append(" (${String.format("%09.2f", time)})\n")
                } else {
                    builder.append(" ")
                }
                time += v
            }
            builder.append("\n")
            return builder.toString()
        } else {
            return "No flux data found"
        }
    }

    @ShellMethod(value = "Show track sector skews", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun skew(): String {
        val sb = StringBuilder()

        // print header
        sb.append(AnsiOutput.toString(AnsiColor.YELLOW, "    0ms                                                                                                    208.3ms\n", AnsiColor.DEFAULT))
        sb.append(AnsiOutput.toString(AnsiColor.YELLOW, "     |                                                                                                        |\n", AnsiColor.DEFAULT))

        // print track rows
        loadedDisk!!.tracks.forEach {
            val sectors = Array<Sector?>(107, { null })
            it.sectors.forEach {
                sectors[convertSectorPositionToIndex(it.position)] = it
            }
            sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_YELLOW, "T${String.format("%02d", it.number)}: "))
            sectors.forEach {
                if (it != null) {
                    sb.append(createSectorString(it))
                } else {
                    sb.append(AnsiOutput.toString(AnsiColor.BLUE, "-"))
                }
            }
            sb.append(AnsiOutput.toString(AnsiColor.DEFAULT, "\n"))
        }

        return sb.toString()
    }

    @ShellMethod(value = "Show track sector interleave", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun interleave(@ShellOption(help = "The track to start from", defaultValue = "0")startTrack: Int, @ShellOption(help = "The track to end with inclusively", defaultValue = "39")endTrack: Int, @ShellOption(help = "Display angular position of each sector", defaultValue = "false")showPosition: String): String {
        val sb = StringBuilder()
        val sectorsPerTrack = loadedDisk!!.sectorsPerTrack
        val show = "true".equals(showPosition, true)

        if (startTrack < 0 || startTrack >= loadedDisk!!.trackCount) {
            sb.append("Invalid starting track");
        } else {
            if (endTrack < startTrack || endTrack >= loadedDisk!!.trackCount) {
                sb.append("Invalid ending track");
            } else {
                for (i in startTrack..endTrack) {
                    val it = loadedDisk!!.getTrack(i)
                    sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_YELLOW, "T${String.format("%02d", it.number)} (${String.format("%04d", it.number * sectorsPerTrack + 1)}-${String.format("%04d", it.number * sectorsPerTrack + sectorsPerTrack)}): "))
                    it.sectors.forEach {
                        sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, "${convertSectorNumberToLetter(it.number)}"))
                        if (show) {
                            sb.append(AnsiOutput.toString(AnsiColor.BLUE, " ${String.format("%05d", it.position)} "))
                        }
                    }
                    sb.append(AnsiOutput.toString(AnsiColor.DEFAULT, "\n"))
                }
            }
        }

        return sb.toString()
    }

    @ShellMethod(value = "Show description of all sectors", group = "Sector")
    @ShellMethodAvailability("isDiskLoaded")
    fun sectorDesc(): String {
        val sectorsPerTrack = loadedDisk!!.sectorsPerTrack
        val maxSectorCount = sectorsPerTrack * loadedDisk!!.trackCount
        val sb = StringBuilder()

        for (i in 1..maxSectorCount) {
            val sectorList = loadedDisk!!.getSector(i)
            var desc: String? = null

            if (sectorList.size == 1) {
                desc = sectorList.first().description
            }

            if (desc == null) {
                desc = "Unknown"
            }

            sb.append("S${String.format("%03d", i)}: $desc\n")
        }

        return sb.toString()
    }

    @ShellMethod(value = "Show summary of all sectors", group = "Sector")
    @ShellMethodAvailability("isDiskLoaded")
    fun sectorSummary(): String {
        val sectorsPerTrack = loadedDisk!!.sectorsPerTrack
        val maxSectorCount = sectorsPerTrack * loadedDisk!!.trackCount
        val sb = StringBuilder(AnsiOutput.toString(AnsiColor.BRIGHT_YELLOW, "T${String.format("%02d", 0)} (0001-${String.format("%04d", sectorsPerTrack)}): "))

        for (i in 1..maxSectorCount) {
            val sectorList = loadedDisk!!.getSector(i)

            if (sectorList.isEmpty()) {
                sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_BLUE, "B"))
            } else if (sectorList.size > 1) {
                sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, "P"))
            } else {
                val sector = sectorList.first()
                if (sector.isEmpty()) {
                    if (sector.hasError) {
                        sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, "e"))
                    } else {
                        sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, "."))
                    }
                } else {
                    if (sector.hasError) {
                        sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, "E"))
                    } else {
                        sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, "O"))
                    }
                }
            }

            if (i % sectorsPerTrack == 0) {
                sb.append("\n")
                if (i < maxSectorCount) {
                    val trackNum = getTrackNumberForAbsoluteSectorNumber(i + 1, sectorsPerTrack)
                    sb.append(AnsiOutput.toString(AnsiColor.BRIGHT_YELLOW, "T${String.format("%02d", trackNum)} (${String.format("%04d", trackNum * sectorsPerTrack + 1)}-${String.format("%04d", trackNum * sectorsPerTrack + sectorsPerTrack)}): "))
                }
            }
        }
        sb.append(AnsiOutput.toString(AnsiColor.DEFAULT, "\nKEY: '.' empty; 'e' empty with error, 'O' has data; 'E' data with error, 'B' missing; , 'P' phantom\n\n"))

        return sb.toString()
    }

    @ShellMethod(value = "Show sector details", group = "Sector")
    @ShellMethodAvailability("isDiskLoaded")
    fun sector(@ShellOption(help = "Absolute sector number (1-indexed integer or tXXsXX string)")number: String, @ShellOption(help = "Show character dump")showChar: Boolean, @ShellOption(help = "Show Base64 dump")showBase64: Boolean): String {
        var found = false
        val sb = StringBuilder()
        var abnum = getAbsoluteSectorForTrackAndSectorString(number, loadedDisk!!.sectorsPerTrack)
        if (abnum == null) {
            abnum = number.toInt()
        }

        val tgtTrack = getTrackNumberForAbsoluteSectorNumber(abnum, loadedDisk!!.sectorsPerTrack)
        val tgtSector = getTrackSectorNumberForAbsoluteSectorNumber(abnum, loadedDisk!!.sectorsPerTrack)

        val crc32 = CRC32()

        val track = loadedDisk!!.tracks[tgtTrack]
        track.sectors.forEach {
            if (it.number == tgtSector) {
                sb.append("Found sector number $tgtSector on track $tgtTrack (${getAbsoluteSectorForTrackAndSectorIndex(tgtTrack, tgtSector, loadedDisk!!.sectorsPerTrack)}) at position ${it.position} with FDC status ${String.format("$%02x",it.fdcStatus)}\nData CRC (full);     (${String.format("%08x", it.dataCrc.value)})\nData CRC (-3 bytes): (${String.format("%08x", it.dataCrcMinus3.value)})\n")
                if (it.hasData) {
                    sb.append("File number: ${it.data!![125].toInt() shr 2}\n")
                    sb.append("Next sector: ${calculateNextSector(it.data!![125].toUByte(), it.data!![126].toUByte())}\n")
                    sb.append("Bytes in sector: ${it.data!![127]}\n")
                }
                if (it.crcError) {
                    sb.append("CRC error flag\n")
                }
                if (it.isDeleted) {
                    sb.append("Deleted sector flag\n")
                }
                if (it.isMissing) {
                    sb.append("Missing sector flag\n")
                }
                if (it.hasWeakData) {
                    sb.append("Weak data found starting at offset ${it.weakOffset}\n")
                }
                if (it.isLong) {
                    sb.append("Long sector detected\n")
                }

                if (it.hasDescription) {
                    sb.append("Description: ${it.description}\n")
                }

                if (it.data != null) {
                    sb.append("\nHex Dump\n--------\n")
                    it.data!!.forEachIndexed { index, value ->
                        sb.append(String.format("%02X ", value))
                        crc32.update(value.toInt())
                        if ((index + 1) % 16 == 0) {
                            sb.append("  (${createCRC32BString(crc32)})\n")
                            crc32.reset()
                        }
                    }
                    if (showChar) {
                        sb.append("\nCharacter Dump\n--------------\n")
                        it.data!!.forEachIndexed { index, value ->
                            sb.append(String.format(" %c ", value.toChar()))
                            if ((index + 1) % 16 == 0) {
                                sb.append("\n")
                            }
                        }
                        sb.append("\n")
                    }
                    if (showBase64) {
                        sb.append("\nBase64 Dump\n-----------\n${Base64.getEncoder().encodeToString(it.data)}\n")
                    }
                } else {
                    sb.append("NO SECTOR DATA\n")
                }

                found = true
            }
        }

        if (!found) {
            sb.append("Unable to find sector $tgtSector on track $tgtTrack\n")
        }

        return sb.toString()
    }

    @ShellMethod(value = "Search for text on disk", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun search(text: String): String {
        val builder = StringBuilder()
        for (track in loadedDisk!!.tracks) {
            for (sector in track.sectors) {
                if (sector.hasData) {
                    if (String(sector.data!!).indexOf(text) > -1) {
                        builder.append("Found text in track ${track.number}, sector ${sector.number} (${getAbsoluteSectorForTrackAndSectorIndex(track.number, sector.number, loadedDisk!!.sectorsPerTrack)})\n")
                    }
                }
            }
        }
        return builder.toString()
    }

    @ShellMethod(value = "Calculate CRC for range of sector data", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun sectorCrc(@ShellOption(help = "Absolute start sector number (1-indexed)")start: Int, @ShellOption(help = "Number of sectors")count: Int): String {
        return createCRC32StringFromSectors(loadedDisk!!, start, count)
    }

    @ShellMethod(value = "List CRCs for sector range", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun sectorCrcList(start: Int, count: Int, name: String): String {
        val builder = StringBuilder()
        var ix = 1
        for (i in start until start+count) {
            builder.append("\"${String.format("%08x", loadedDisk!!.getSector(i).first().dataCrcMinus3.value)}\" -> \"$name $ix\"\n")
            ix++
        }
        return builder.toString()
    }

    @ShellMethod(value = "Save disk as ATX file", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun saveATX(filename: String): String {
        writeATX(File(filename), loadedDisk!!)
        return "Saved"
    }

    @ShellMethod(value = "Save disk as ATR file", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun saveATR(filename: String): String {
        writeATR(File(filename), loadedDisk!!)
        return "Saved"
    }

    @ShellMethod(value = "Save disk as JSON file", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun saveJSON(filename: String): String {
        writeDiskToJSON(File(filename), objectMapper!!, loadedDisk!!)
        return "Saved"
    }

    @ShellMethod(value = "Save sector as JSON file", group = "Disk")
    @ShellMethodAvailability("isDiskLoaded")
    fun saveSectorJSON(@ShellOption(help = "Filename to save") filename: String, @ShellOption(help = "Sector number") number: Int): String {
        writeSectorsToJSON(File(filename), objectMapper!!, loadedDisk!!, number)
        return "Saved"
    }

    @ShellMethod(value = "Set weak data offset for sector", group = "Sector")
    @ShellMethodAvailability("isDiskLoaded")
    fun setWeak(@ShellOption(help = "Absolute sector number (1-indexed)") sector: String, @ShellOption(help = "Starting offset of weak data (0-indexed, -1 to clear)")offset: Int): String {
        return alterAbsoluteSector(loadedDisk!!, sector) {
            if (offset != -1) it.weakOffset = offset else it.weakOffset = null
        }
    }

    @ShellMethod(value = "Set status for sector", group = "Sector")
    @ShellMethodAvailability("isDiskLoaded")
    fun setSectorStatus(@ShellOption(help = "Absolute sector number (1-indexed)") sector: String, status: Int): String {
        return alterAbsoluteSector(loadedDisk!!, sector) {
            it.fdcStatus = status
        }
    }

    @ShellMethod(value = "Set bytes for sector", group = "Sector")
    @ShellMethodAvailability("isDiskLoaded")
    fun setSectorBytes(@ShellOption(help = "Absolute sector number (1-indexed)") sector: String, @ShellOption(help = "Starting index in sector for new bytes (0-index)") offset: Int, @ShellOption(help = "The new bytes to set (as hex values with no spaces)") data: String): String {
        if (data.length % 2 != 0) {
            return "Data string has uneven number of characters"
        }
        if (offset < 0) {
            return "Offset cannot be less than 0"
        }
        val byteCount = data.length / 2
        if (loadedDisk!!.sectorSize != null && offset + byteCount > loadedDisk!!.sectorSize!!) {
            return "Number of bytes cannot exceed sector size"
        }

        val (num,pos) = parseSectorPositionString(sector)
        val sectors = loadedDisk!!.getSector(num)

        if (sectors.size > 1) {
            return "Multiple sectors found - unable to set bytes"
        }
        if (sectors[0].data == null) {
            return "No data found for sector"
        }

        val newBytes = hexStringToByteArray(data)
        for ((i, b) in newBytes.withIndex()) {
            sectors[0].data!![i+offset] = b
        }

        return "OK"
    }

    @ShellMethod(value = "Toggle CRC error for sector", group = "Sector")
    @ShellMethodAvailability("isDiskLoaded")
    fun toggleCRC(@ShellOption(help = "Absolute sector number (1-indexed)") sector: String): String {
        return alterAbsoluteSector(loadedDisk!!, sector) {
            it.crcError = !it.crcError
        }
    }

    @ShellMethod(key = arrayOf("set-810-interleave"), value = "Set 810 sector interleave for track(s)", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun set810Interleave(@ShellOption(help = "Track number (0-39 or 99 for all)") trackNum: Int): String {
        return setCustomInterleave(trackNum, "I7E3AH6D29G5C18F4B")
    }

    @ShellMethod(key = arrayOf("set-1050-interleave"), value = "Set 1050 sector interleave for track(s)", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun set1050Interleave(@ShellOption(help = "Track number (0-39 or 99 for all)") trackNum: Int): String {
        return setCustomInterleave(trackNum, "13579BDFH2468ACEGI")
    }

    @ShellMethod(value = "Set custom sector interleave for track(s)", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun setCustomInterleave(@ShellOption(help = "Track number (0-39 or 99 for all)") trackNum: Int, @ShellOption(help = "Sector interleave in Base 18 format (e.g. \"1234567890ABCDEFGH\")")interleave: String): String {
        if (trackNum == 99) {
            for (t in loadedDisk!!.tracks) {
                t.setInterleave(interleave)
            }
        } else {
            loadedDisk!!.tracks.get(trackNum).setInterleave(interleave)
        }
        return "OK"
    }

    @ShellMethod(value = "Delete the last track of the disk", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun deleteLastTrack(): String {
        loadedDisk!!.deleteLastTracks(1)
        return "OK"
    }

    @ShellMethod(value = "Renumber all tracks of disk starting with 0", group = "Track")
    @ShellMethodAvailability("isDiskLoaded")
    fun renumberTracks(): String {
        loadedDisk!!.renumberTracks()
        return "OK"
    }

    @ShellMethod(value = "Load repair disk image file", group = "Repair")
    @ShellMethodAvailability("isDiskLoaded")
    fun repairLoad(@ShellOption file: File): String {
        // make sure the files exist
        if (!file.isFile) {
            return "${file.absolutePath} ${strings.getString("notFoundSuffix")}"
        }

        // read the disk image
        repairDisks.add(readDiskImage(file, objectMapper!!))

        return "Successfully loaded repair image file.\n"
    }

    @ShellMethod(value = "Clear all repair disk image files", group = "Repair")
    @ShellMethodAvailability("isRepairAvailable")
    fun repairClear(): String {
        repairDisks.clear()
        return "Successfully cleared repair images.\n"
    }

    @ShellMethod(value = "Show all loaded repair image files", group = "Repair")
    @ShellMethodAvailability("isRepairAvailable")
    fun repairSummary(): String {
        return if (repairDisks.size > 0) {
            "${repairDisks.size} repair images have been loaded.\n"
        } else {
            "No repair images have been loaded.\n"
        }
    }

    @ShellMethod(value = "Show repair plan for loaded image", group = "Repair")
    @ShellMethodAvailability("isRepairAvailable")
    fun repairPlan(): String {
        val repairPlan = createRepairPlan(loadedDisk!!, repairDisks)
        val builder = StringBuilder()
        var count = 0

        for (srp in repairPlan.sectorRepairPlans) {
            builder.append("S${String.format("%03d", srp.number)} can ${if (srp.isFullyRepairable()) "be" else "be partially"} repaired\n")
            count++
        }

        builder.append("\n$count total sectors are repairable.\n")

        return builder.toString()
    }

    @ShellMethod(value = "Apply repairs for loaded image", group = "Repair")
    @ShellMethodAvailability("isRepairAvailable")
    fun repairApply(): String {
        val builder = StringBuilder()
        applyRepairPlan(loadedDisk!!, createRepairPlan(loadedDisk!!, repairDisks), builder)
        return builder.toString()
    }

    fun isDiskLoaded(): Availability {
        return if (loadedDisk != null) Availability.available() else Availability.unavailable("no disk image has loaded.")
    }

    fun isRepairAvailable(): Availability {
        return if (repairDisks.size > 0) Availability.available() else Availability.unavailable("no repair images have been loaded.")
    }
}
