import com.a8preservation.disklib.*
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.springframework.boot.ansi.AnsiColor
import org.springframework.boot.ansi.AnsiOutput
import java.io.File
import java.io.IOException
import java.util.*
import java.util.zip.CRC32

/**
 * Read an Atari disk image.
 *
 * Reads the contents of an Atari 8-bit disk image file and returns a representative Disk object.
 *
 * @param file the disk image file
 */
fun readDiskImage(file: File, objectMapper: ObjectMapper): Disk {
    return when (file.extension.toLowerCase()) {
        "atx" -> readATX(file.inputStream(), file.name)
        "atr" -> readATR(file.inputStream(), file.name)
        "pro" -> readPRO(file.inputStream(), null, file.name)
        "json" -> readJSON(file, objectMapper)
        "scp" -> readSCP(file.inputStream())
        else -> throw IOException("Unsupported file extension: ${file.extension}")
    }
}

fun convertSectorPositionToIndex(pos: Int): Int {
    return (pos - 1) / 250
}

fun createSectorString(sector: Sector?): String {
    return if (sector != null) {
        val c = convertSectorNumberToLetter(sector.number)
        when {
            sector.number == 1 -> AnsiOutput.toString(AnsiColor.BRIGHT_RED, c.toString())
            sector.hasError -> AnsiOutput.toString(AnsiColor.BRIGHT_YELLOW, c.toString())
            else -> AnsiOutput.toString(AnsiColor.BRIGHT_GREEN, c.toString())
        }
    } else {
        AnsiOutput.toString(AnsiColor.BLUE, "-")
    }
}

fun calculateNextSector(b1: UByte, b2: UByte): UInt {
    return ((b1 and 3u) * 256u) + b2
}

@Throws(NumberFormatException::class)
fun parseSectorPositionString(str: String): List<Int> {
    return if (str.indexOf(".") > -1) {
        str.split(".").map{ it.toInt() }
    } else {
        listOf(str.toInt(), -1)
    }
}

fun alterAbsoluteSector(disk: Disk, numPos: String, func: (Sector) -> Unit): String {
    val (num,pos) = parseSectorPositionString(numPos)
    val sectors = disk.getSector(num)

    return when {
        sectors.size > 1 -> {
            return if (pos > -1) {
                sectors.filter { it.position == pos}.forEach { func(it) }
                "Sector has been successfully changed"
            } else {
                "Multiple sectors found for $num. Please specify {sector}.{position} (e.g. " + sectors.joinToString{ "$num.${it.position}" } + ")"
            }
        }
        sectors.size == 1 -> {
            func(sectors[0])
            "Sector has been successfully changed"
        }
        else -> "Sector $num not found"
    }
}

fun createCRC32StringFromSectors(disk: Disk, start: Int, count: Int): String {
    val crc = CRC32()
    var nextSector = start
    var currCount = count
    while (nextSector > 0) {
        if (currCount < 0) {
            return "Broken DOS file chain"
        }
        val l = disk.getSector(nextSector)
        if (l.size == 1) {
            val s = l[0]
            if (s.hasData) {
                crc.update(s.data!!.sliceArray(IntRange(0, s.data!![127].toInt())))
            }
            nextSector = calculateNextSector(s.data!![125].toUByte(), s.data!![126].toUByte()).toInt()
            currCount--
        } else {
            return "Multiple sectors found for $nextSector; unable to perform CRC calculation"
        }
    }
    return String.format("%08x", crc.value)
}

fun readJSON(file: File, objectMapper: ObjectMapper): Disk {
    val rootNode = objectMapper.readTree(file)
    val tit: Iterator<JsonNode> = rootNode.withArray("tracks").elements()
    val tracks = mutableListOf<Track>()
    while (tit.hasNext()) {
        val tnode = tit.next()
        val sit: Iterator<JsonNode> = tnode.withArray("sectors").elements()
        val sectors = mutableListOf<Sector>()
        while (sit.hasNext()) {
            val snode = sit.next()
            val sector = Sector(snode.get("number").asInt(), Base64.getDecoder().decode(snode.get("data").asText()), snode.get("position").asInt(), snode.get("fdcStatus").asInt())
            if (snode.has("weakOffset")) {
                sector.weakOffset = snode.get("weakOffset").intValue()
            }
            sectors.add(sector)
        }
        tracks.add(Track(tnode.get("number").asInt(), TrackEncoding.valueOf(tnode.get("encoding").asText()), sectors))
    }
    return Disk(DiskDensity.valueOf(rootNode.get("density").textValue()), tracks)
}

fun writeDiskToJSON(file: File, objectMapper: ObjectMapper, disk: Disk) {
    val rootNode = objectMapper.createObjectNode()
    rootNode.put("density", disk.density.name)
    val tracksNode = objectMapper.createArrayNode()
    rootNode.set("tracks", tracksNode)
    for (t in disk.tracks) {
        val trackNode = objectMapper.createObjectNode()
        tracksNode.add(trackNode)
        trackNode.put("number", t.number)
        trackNode.put("encoding", t.encoding.name)
        val sectorsNode = objectMapper.createArrayNode()
        trackNode.set("sectors", sectorsNode)
        for (s in t.sectors) {
            val sectorNode = objectMapper.createObjectNode()
            sectorsNode.add(sectorNode)
            createSectorJSON(sectorNode, t.number, s, disk.sectorsPerTrack)
        }
    }
    file.writeText(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode))
}

fun writeSectorsToJSON(file: File, objectMapper: ObjectMapper, disk: Disk, sectorNumber: Int) {
    val rootNode = objectMapper.createArrayNode()
    for (s in disk.getSector(sectorNumber)) {
        val sectorNode = objectMapper.createObjectNode()
        rootNode.add(sectorNode)
        createSectorJSON(sectorNode, getTrackNumberForAbsoluteSectorNumber(sectorNumber, disk.sectorsPerTrack), s, disk.sectorsPerTrack)
    }
    file.writeText(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode))
}

fun createSectorJSON(sectorNode: ObjectNode, trackNumber: Int, s: Sector, sectorsPerTrack: Int) {
    sectorNode.put("absNumber", getAbsoluteSectorForTrackAndSectorIndex(trackNumber, s.number, sectorsPerTrack))
    sectorNode.put("number", s.number)
    sectorNode.put("position", s.position)
    sectorNode.put("fdcStatus", s.fdcStatus)
    if (s.hasWeakData) {
        sectorNode.put("weakOffset", s.weakOffset)
    }
    sectorNode.put("data", Base64.getEncoder().encodeToString(s.data))
}

fun detectSectorDescription(crc32: String): String? {
    return when (crc32) {
        "dba174a8" -> "Empty sector"

        "c80c5ea8" -> "DOS 2.0s boot sector 2"
        "6c89f1bc" -> "DOS 2.0s boot sector 3"
        "2b4289af" -> "DOS 2.0S DOS.SYS sector 1"
        "049a3d84" -> "DOS 2.0S DOS.SYS sector 2"
        "b9562d10" -> "DOS 2.0S DOS.SYS sector 3"
        "be963412" -> "DOS 2.0S DOS.SYS sector 4"
        "bda37c7e" -> "DOS 2.0S DOS.SYS sector 5"
        "8713c30a" -> "DOS 2.0S DOS.SYS sector 6"
        "cc15bca7" -> "DOS 2.0S DOS.SYS sector 7"
        "5b17d2b3" -> "DOS 2.0S DOS.SYS sector 8"
        "0d935c0c" -> "DOS 2.0S DOS.SYS sector 9"
        "153c7389" -> "DOS 2.0S DOS.SYS sector 10"
        "5d71844f" -> "DOS 2.0S DOS.SYS sector 11"
        "195b11fa" -> "DOS 2.0S DOS.SYS sector 12"
        "2b35451e" -> "DOS 2.0S DOS.SYS sector 13"
        "5ca4ea55" -> "DOS 2.0S DOS.SYS sector 14"
        "d70806fc" -> "DOS 2.0S DOS.SYS sector 15"
        "19ee2ebb" -> "DOS 2.0S DOS.SYS sector 16"
        "ad6aa4e4" -> "DOS 2.0S DOS.SYS sector 17"
        "bb30e98f" -> "DOS 2.0S DOS.SYS sector 18"
        "eda9efac" -> "DOS 2.0S DOS.SYS sector 19"
        "7caf377f" -> "DOS 2.0S DOS.SYS sector 20"
        "3d6bdb13" -> "DOS 2.0S DOS.SYS sector 21"
        "f7851ab2" -> "DOS 2.0S DOS.SYS sector 22"
        "765e5f6f" -> "DOS 2.0S DOS.SYS sector 23"
        "38b2f471" -> "DOS 2.0S DOS.SYS sector 24"
        "9bf7feeb" -> "DOS 2.0S DOS.SYS sector 25"
        "502ed764" -> "DOS 2.0S/2.5 DOS.SYS sector 26"
        "ad9a63d2" -> "DOS 2.0S DOS.SYS sector 27"
        "5612c287" -> "DOS 2.0S DOS.SYS sector 28"
        "55f8ce6b" -> "DOS 2.0S DOS.SYS sector 29"
        "49796e4d" -> "DOS 2.0S DOS.SYS sector 30"
        "9f08aeb7" -> "DOS 2.0S DOS.SYS sector 31"
        "ac618a7a" -> "DOS 2.0S DOS.SYS sector 32"
        "2cd9d3ee" -> "DOS 2.0S DOS.SYS sector 33"
        "64371c44" -> "DOS 2.0S DOS.SYS sector 34"
        "fb3e99fb" -> "DOS 2.0S DOS.SYS sector 35"
        "6a98876a" -> "DOS 2.0S DOS.SYS sector 36"
        "20e10ef8" -> "DOS 2.0S DOS.SYS sector 37"
        "1e32d463" -> "DOS 2.0S DOS.SYS sector 38"
        "f2354686" -> "DOS 2.0S DOS.SYS sector 39"
        "da1798bc" -> "DOS 2.0S DUP.SYS sector 1"
        "2f35ad88" -> "DOS 2.0S DUP.SYS sector 2"
        "8f730479" -> "DOS 2.0S DUP.SYS sector 3"
        "65a06957" -> "DOS 2.0S DUP.SYS sector 4"
        "bcf4527a" -> "DOS 2.0S DUP.SYS sector 5"
        "db33fc48" -> "DOS 2.0S DUP.SYS sector 6"
        "39414f59" -> "DOS 2.0S DUP.SYS sector 7"
        "470f572a" -> "DOS 2.0S DUP.SYS sector 8"
        "a824d119" -> "DOS 2.0S DUP.SYS sector 9"
        "2e4c2c8f" -> "DOS 2.0S DUP.SYS sector 10"
        "57cc16a0" -> "DOS 2.0S DUP.SYS sector 11"
        "98565a4b" -> "DOS 2.0S DUP.SYS sector 12"
        "ab7b3e89" -> "DOS 2.0S DUP.SYS sector 13"
        "209401e0" -> "DOS 2.0S DUP.SYS sector 14"
        "1cbf0370" -> "DOS 2.0S DUP.SYS sector 15"
        "59b66344" -> "DOS 2.0S DUP.SYS sector 16"
        "05985cc8" -> "DOS 2.0S DUP.SYS sector 17"
        "e30b8e9c" -> "DOS 2.0S DUP.SYS sector 18"
        "22bd929a" -> "DOS 2.0S DUP.SYS sector 19"
        "297807de" -> "DOS 2.0S DUP.SYS sector 20"
        "5892e66f" -> "DOS 2.0S DUP.SYS sector 21"
        "9d12d12c" -> "DOS 2.0S DUP.SYS sector 22"
        "9b17a02d" -> "DOS 2.0S DUP.SYS sector 23"
        "894cc2be" -> "DOS 2.0S DUP.SYS sector 24"
        "99eeb350" -> "DOS 2.0S DUP.SYS sector 25"
        "6178e6f2" -> "DOS 2.0S DUP.SYS sector 26"
        "a12874b5" -> "DOS 2.0S DUP.SYS sector 27"
        "8e443304" -> "DOS 2.0S DUP.SYS sector 28"
        "e10c60d9" -> "DOS 2.0S DUP.SYS sector 29"
        "0fe5f113" -> "DOS 2.0S DUP.SYS sector 30"
        "0794c18d" -> "DOS 2.0S DUP.SYS sector 31"
        "acebc6cb" -> "DOS 2.0S DUP.SYS sector 32"
        "d1921418" -> "DOS 2.0S DUP.SYS sector 33"
        "ebef46bf" -> "DOS 2.0S DUP.SYS sector 34"
        "9e915113" -> "DOS 2.0S DUP.SYS sector 35"
        "d066b925" -> "DOS 2.0S DUP.SYS sector 36"
        "00624fbc" -> "DOS 2.0S DUP.SYS sector 37"
        "0dbea2df" -> "DOS 2.0S DUP.SYS sector 38"
        "f632a126" -> "DOS 2.0S DUP.SYS sector 39"
        "9b6ed2f8" -> "DOS 2.0S DUP.SYS sector 40"
        "dca84d33" -> "DOS 2.0S DUP.SYS sector 41/42"

        "fdcccbfd" -> "DOS 2.5 boot sector 2"
        "2e4f0303" -> "DOS 2.5 boot sector 3"
        "5b86ea69" -> "DOS 2.5 DOS.SYS sector 1"
        "a28ac178" -> "DOS 2.5 DOS.SYS sector 2"
        "9ea196e6" -> "DOS 2.5 DOS.SYS sector 3"
        "40a4c435" -> "DOS 2.5 DOS.SYS sector 4"
        "678a4189" -> "DOS 2.5 DOS.SYS sector 5"
        "3347ff26" -> "DOS 2.5 DOS.SYS sector 6"
        "89616792" -> "DOS 2.5 DOS.SYS sector 7"
        "6288bd4f" -> "DOS 2.5 DOS.SYS sector 8"
        "ff033a33" -> "DOS 2.5 DOS.SYS sector 9"
        "27e890e4" -> "DOS 2.5 DOS.SYS sector 10"
        "09642297" -> "DOS 2.5 DOS.SYS sector 11"
        "38a7fb87" -> "DOS 2.5 DOS.SYS sector 12"
        "ebdd31f2" -> "DOS 2.5 DOS.SYS sector 13"
        "ae67ddd2" -> "DOS 2.5 DOS.SYS sector 14"
        "8aae52ef" -> "DOS 2.5 DOS.SYS sector 15"
        "c4607b3f" -> "DOS 2.5 DOS.SYS sector 16"
        "f718c58b" -> "DOS 2.5 DOS.SYS sector 17"
        "579d3070" -> "DOS 2.5 DOS.SYS sector 18"
        "90c00103" -> "DOS 2.5 DOS.SYS sector 19"
        "c4d3ed0f" -> "DOS 2.5 DOS.SYS sector 20"
        "ed26a0a8" -> "DOS 2.5 DOS.SYS sector 21"
        "8d501d47" -> "DOS 2.5 DOS.SYS sector 22"
        "8cda92d6" -> "DOS 2.5 DOS.SYS sector 23"
        "eb0df32a" -> "DOS 2.5 DOS.SYS sector 24"
        "fbecaa78" -> "DOS 2.5 DOS.SYS sector 25"
        "22a6935c" -> "DOS 2.5 DOS.SYS sector 27"
        "4baef7b3" -> "DOS 2.5 DOS.SYS sector 28"
        "013765a2" -> "DOS 2.5 DOS.SYS sector 29"
        "2d9c38ee" -> "DOS 2.5 DOS.SYS sector 30"
        "0fac9d85" -> "DOS 2.5 DOS.SYS sector 31"
        "24739bf1" -> "DOS 2.5 DOS.SYS sector 32"
        "f394f38a" -> "DOS 2.5 DOS.SYS sector 33"
        "5ac8228f" -> "DOS 2.5 DOS.SYS sector 34"
        "88dd15c3" -> "DOS 2.5 DOS.SYS sector 35"
        "a5539c42" -> "DOS 2.5 DOS.SYS sector 36"
        "4631810d" -> "DOS 2.5 DOS.SYS sector 37"
        "b414041d" -> "DOS 2.5 DUP.SYS sector 1"
        "d81e040c" -> "DOS 2.5 DUP.SYS sector 2"
        "75aec72b" -> "DOS 2.5 DUP.SYS sector 3"
        "b3468444" -> "DOS 2.5 DUP.SYS sector 4"
        "42bbab35" -> "DOS 2.5 DUP.SYS sector 5"
        "3fe35336" -> "DOS 2.5 DUP.SYS sector 6"
        "8788c829" -> "DOS 2.5 DUP.SYS sector 7"
        "6bdbe262" -> "DOS 2.5 DUP.SYS sector 8"
        "3badd786" -> "DOS 2.5 DUP.SYS sector 9"
        "105977f6" -> "DOS 2.5 DUP.SYS sector 10"
        "73f573c6" -> "DOS 2.5 DUP.SYS sector 11"
        "a0ee003c" -> "DOS 2.5 DUP.SYS sector 12"
        "caff58e1" -> "DOS 2.5 DUP.SYS sector 13"
        "7e9720f9" -> "DOS 2.5 DUP.SYS sector 14"
        "baf4facb" -> "DOS 2.5 DUP.SYS sector 15"
        "85cad45e" -> "DOS 2.5 DUP.SYS sector 16"
        "fad83675" -> "DOS 2.5 DUP.SYS sector 17"
        "7978c886" -> "DOS 2.5 DUP.SYS sector 18"
        "92c5209a" -> "DOS 2.5 DUP.SYS sector 19"
        "29cd5bf0" -> "DOS 2.5 DUP.SYS sector 20"
        "5d85b53a" -> "DOS 2.5 DUP.SYS sector 21"
        "0a59a5b9" -> "DOS 2.5 DUP.SYS sector 22"
        "d2b36c26" -> "DOS 2.5 DUP.SYS sector 23"
        "ba1108cd" -> "DOS 2.5 DUP.SYS sector 24"
        "1d7896e2" -> "DOS 2.5 DUP.SYS sector 25"
        "ff5f095c" -> "DOS 2.5 DUP.SYS sector 26"
        "93ceb8f4" -> "DOS 2.5 DUP.SYS sector 27"
        "87788add" -> "DOS 2.5 DUP.SYS sector 28"
        "fb864481" -> "DOS 2.5 DUP.SYS sector 29"
        "6565b7b1" -> "DOS 2.5 DUP.SYS sector 30"
        "0a54a079" -> "DOS 2.5 DUP.SYS sector 31"
        "24d1308f" -> "DOS 2.5 DUP.SYS sector 32"
        "d1ba0dfe" -> "DOS 2.5 DUP.SYS sector 33"
        "08d6d5ca" -> "DOS 2.5 DUP.SYS sector 34"
        "7f8a07ea" -> "DOS 2.5 DUP.SYS sector 35"
        "e172f097" -> "DOS 2.5 DUP.SYS sector 36"
        "08653c1e" -> "DOS 2.5 DUP.SYS sector 37"
        "8c961c69" -> "DOS 2.5 DUP.SYS sector 38"
        "80b248e3" -> "DOS 2.5 DUP.SYS sector 39"
        "d2e51d52" -> "DOS 2.5 DUP.SYS sector 40"
        "35acb907" -> "DOS 2.5 DUP.SYS sector 41"
        "ebf1b48f" -> "DOS 2.5 DUP.SYS sector 42"

        "8d21d267" -> "Turbo BASIC XL 1.5 Runtime 1"
        "5228db2b" -> "Turbo BASIC XL 1.5 Runtime 2"
        "e90c0338" -> "Turbo BASIC XL 1.5 Runtime 3"
        "ecf0d482" -> "Turbo BASIC XL 1.5 Runtime 4"
        "583121a7" -> "Turbo BASIC XL 1.5 Runtime 5"
        "d22b540e" -> "Turbo BASIC XL 1.5 Runtime 6"
        "ca279111" -> "Turbo BASIC XL 1.5 Runtime 7"
        "5508a88d" -> "Turbo BASIC XL 1.5 Runtime 8"
        "1ce96ad2" -> "Turbo BASIC XL 1.5 Runtime 9"
        "e165fae7" -> "Turbo BASIC XL 1.5 Runtime 10"
        "9cf4cae6" -> "Turbo BASIC XL 1.5 Runtime 11"
        "1a14afcc" -> "Turbo BASIC XL 1.5 Runtime 12"
        "0e7771dd" -> "Turbo BASIC XL 1.5 Runtime 13"
        "14d50453" -> "Turbo BASIC XL 1.5 Runtime 14"
        "e6e1e16b" -> "Turbo BASIC XL 1.5 Runtime 15"
        "ae8636db" -> "Turbo BASIC XL 1.5 Runtime 16"
        "16e57edd" -> "Turbo BASIC XL 1.5 Runtime 17"
        "cfd0400d" -> "Turbo BASIC XL 1.5 Runtime 18"
        "d3971d60" -> "Turbo BASIC XL 1.5 Runtime 19"
        "6c2d230a" -> "Turbo BASIC XL 1.5 Runtime 20"
        "909e643e" -> "Turbo BASIC XL 1.5 Runtime 21"
        "53d6e16c" -> "Turbo BASIC XL 1.5 Runtime 22"
        "8b21b855" -> "Turbo BASIC XL 1.5 Runtime 23"
        "38c290ba" -> "Turbo BASIC XL 1.5 Runtime 24"
        "f257c0eb" -> "Turbo BASIC XL 1.5 Runtime 25"
        "ba7e51c2" -> "Turbo BASIC XL 1.5 Runtime 26"
        "0b1bdae7" -> "Turbo BASIC XL 1.5 Runtime 27"
        "aa7dda52" -> "Turbo BASIC XL 1.5 Runtime 28"
        "3c3aca31" -> "Turbo BASIC XL 1.5 Runtime 29"
        "2628ab83" -> "Turbo BASIC XL 1.5 Runtime 30"
        "47a45513" -> "Turbo BASIC XL 1.5 Runtime 31"
        "62aff8b8" -> "Turbo BASIC XL 1.5 Runtime 32"
        "40be4f46" -> "Turbo BASIC XL 1.5 Runtime 33"
        "dcd66909" -> "Turbo BASIC XL 1.5 Runtime 34"
        "87e80109" -> "Turbo BASIC XL 1.5 Runtime 35"
        "ea7bded5" -> "Turbo BASIC XL 1.5 Runtime 36"
        "80609c1f" -> "Turbo BASIC XL 1.5 Runtime 37"
        "88151f59" -> "Turbo BASIC XL 1.5 Runtime 38"
        "f95b4aa1" -> "Turbo BASIC XL 1.5 Runtime 39"
        "c11e5630" -> "Turbo BASIC XL 1.5 Runtime 40"
        "f13a15e6" -> "Turbo BASIC XL 1.5 Runtime 41"
        "54c22f60" -> "Turbo BASIC XL 1.5 Runtime 42"
        "66d70d9e" -> "Turbo BASIC XL 1.5 Runtime 43"
        "657ebe4d" -> "Turbo BASIC XL 1.5 Runtime 44"
        "9dc5fee4" -> "Turbo BASIC XL 1.5 Runtime 45"
        "67eaced1" -> "Turbo BASIC XL 1.5 Runtime 53"
        "9898be23" -> "Turbo BASIC XL 1.5 Runtime 54"
        "0f9e4c88" -> "Turbo BASIC XL 1.5 Runtime 55"
        "94406b6f" -> "Turbo BASIC XL 1.5 Runtime 56"
        "19f7cbbb" -> "Turbo BASIC XL 1.5 Runtime 57"
        "b30abe8b" -> "Turbo BASIC XL 1.5 Runtime 58"
        "8dd0c50d" -> "Turbo BASIC XL 1.5 Runtime 59"
        "c0cd87dc" -> "Turbo BASIC XL 1.5 Runtime 60"
        "f7169181" -> "Turbo BASIC XL 1.5 Runtime 61"
        "02b049c0" -> "Turbo BASIC XL 1.5 Runtime 62"
        "96719ae2" -> "Turbo BASIC XL 1.5 Runtime 63"
        "495f5433" -> "Turbo BASIC XL 1.5 Runtime 64"
        "de65a1e2" -> "Turbo BASIC XL 1.5 Runtime 65"
        "71434563" -> "Turbo BASIC XL 1.5 Runtime 66"
        "33c2edef" -> "Turbo BASIC XL 1.5 Runtime 67"
        "f4eb1937" -> "Turbo BASIC XL 1.5 Runtime 68"
        "cb473dc5" -> "Turbo BASIC XL 1.5 Runtime 69"
        "7b6a4558" -> "Turbo BASIC XL 1.5 Runtime 70"
        "4cda8c12" -> "Turbo BASIC XL 1.5 Runtime 71"
        "92f3a773" -> "Turbo BASIC XL 1.5 Runtime 72"
        "0fde23c1" -> "Turbo BASIC XL 1.5 Runtime 73"
        "10a4175a" -> "Turbo BASIC XL 1.5 Runtime 74"
        "9b5a5745" -> "Turbo BASIC XL 1.5 Runtime 75"
        "545402fe" -> "Turbo BASIC XL 1.5 Runtime 76"
        "d9f450e7" -> "Turbo BASIC XL 1.5 Runtime 77"
        "14a3ef80" -> "Turbo BASIC XL 1.5 Runtime 78"
        "2f695044" -> "Turbo BASIC XL 1.5 Runtime 79"
        "2a116b58" -> "Turbo BASIC XL 1.5 Runtime 80"
        "2041538e" -> "Turbo BASIC XL 1.5 Runtime 81"
        "83039bcb" -> "Turbo BASIC XL 1.5 Runtime 82"
        "a769a6e9" -> "Turbo BASIC XL 1.5 Runtime 83"
        "081a16af" -> "Turbo BASIC XL 1.5 Runtime 84"
        "74922c50" -> "Turbo BASIC XL 1.5 Runtime 85"
        "d6c27cde" -> "Turbo BASIC XL 1.5 Runtime 86"
        "7c82f1e6" -> "Turbo BASIC XL 1.5 Runtime 87"
        "ffac96c4" -> "Turbo BASIC XL 1.5 Runtime 88"

        else -> null
    }
}
