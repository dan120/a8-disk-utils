package com.a8preservation.diskutils.analyzer

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class DiskAnalyzerShell {
}

fun main(args: Array<String>) {
    SpringApplication.run(DiskAnalyzerShell::class.java, *args)
}