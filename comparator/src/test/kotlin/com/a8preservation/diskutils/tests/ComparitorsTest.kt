package com.a8preservation.diskutils.tests

import com.a8preservation.disklib.*
import com.a8preservation.diskutils.comparator.compare
import junit.framework.TestCase.*
import org.junit.Test
import java.io.File
import java.io.FileInputStream
import java.util.*

class ComparitorsTest {
    @Test
    fun `test crc error matching data`() {
        val logger = MockLogger()
        val strings = ResourceBundle.getBundle("strings")
        val b = ByteArray(128) { _ -> 0 }
        val s1 = Sector(1, b, 0, 8)
        assertTrue(s1.crcError)
        assertFalse(s1.isDeleted)
        assertFalse(s1.isMissing)
        val s2 = Sector(1, b, 0, 8)
        assertTrue(s2.crcError)
        assertFalse(s2.isDeleted)
        assertFalse(s2.isMissing)
        assertTrue(s1.compare(1, s2, 18, logger, strings))
        assertEquals(0, logger.messages.size)
    }

    @Test
    fun `test crc error non-matching data`() {
        val logger = MockLogger()
        val strings = ResourceBundle.getBundle("strings")
        val b1 = ByteArray(128) { _ -> 0 }
        val b2 = ByteArray(128) { _ -> 1 }
        val s1 = Sector(1, b1, 0, 8)
        assertTrue(s1.crcError)
        assertFalse(s1.isDeleted)
        assertFalse(s1.isMissing)
        val s2 = Sector(1, b2, 0, 8)
        assertTrue(s2.crcError)
        assertFalse(s2.isDeleted)
        assertFalse(s2.isMissing)
        assertFalse(s1.compare(1, s2, 18, logger, strings))
        assertEquals(1, logger.messages.size)
        assertTrue(logger.messages[0].contains("CRC error with non-matching"))
    }

    @Test
    fun `test matching weak sectors + crc gives no crc error`() {
        val logger = MockLogger()
        val strings = ResourceBundle.getBundle("strings")
        val b1 = ByteArray(128) { _ -> 0 }
        b1[126] = 2
        val b2 = ByteArray(128) { _ -> 0 }
        b2[126] = 3
        val s1 = Sector(1, b1, 0, 8)
        assertTrue(s1.crcError)
        assertFalse(s1.isDeleted)
        assertFalse(s1.isMissing)
        s1.weakOffset = 126
        val s2 = Sector(1, b2, 0, 8)
        assertTrue(s2.crcError)
        assertFalse(s2.isDeleted)
        assertFalse(s2.isMissing)
        s2.weakOffset = 126
        assertTrue(s1.compare(1, s2, 18, logger, strings))
        assertEquals(0, logger.messages.size)
    }

    @Test
    fun `test matching weak sectors with different offsets`() {
        val logger = MockLogger()
        val strings = ResourceBundle.getBundle("strings")
        val b1 = ByteArray(128) { _ -> 0 }
        b1[125] = 2
        b1[126] = 2
        val b2 = ByteArray(128) { _ -> 0 }
        b2[126] = 3
        val s1 = Sector(1, b1, 0, 8)
        assertTrue(s1.crcError)
        assertFalse(s1.isDeleted)
        assertFalse(s1.isMissing)
        s1.weakOffset = 125
        val s2 = Sector(1, b2, 0, 8)
        assertTrue(s2.crcError)
        assertFalse(s2.isDeleted)
        assertFalse(s2.isMissing)
        s2.weakOffset = 126
        assertTrue(s1.compare(1, s2, 18, logger, strings))
        assertEquals(0, logger.messages.size)
    }

    @Test
    fun `test disk compare non-identical ATX`() {
        val disk1 = readATX(FileInputStream(File("./src/test/resources/mythos1-a.atx")))
        val disk2 = readATX(FileInputStream(File("./src/test/resources/mythos1-b.atx")))
        val logger = MockLogger()
        val strings = ResourceBundle.getBundle("strings")
        assertFalse(disk1.compare(disk2, 0, -1, logger, strings))
        assertEquals(4, logger.messages.size)
    }
}