package com.a8preservation.diskutils.tests

import com.a8preservation.diskutils.comparator.LogLevel
import com.a8preservation.diskutils.comparator.Logger

class MockLogger: Logger {
    val messages = ArrayList<String>()

    override fun newline(level: LogLevel) {
    }

    override fun logn(level: LogLevel, msg: String) {
        log(level, msg)
    }

    override fun log(level: LogLevel, msg: String) {
        if (level == LogLevel.ERROR)
            messages.add(msg)
    }
}