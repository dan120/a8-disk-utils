/**
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
 */
package com.a8preservation.diskutils.comparator

import com.a8preservation.disklib.*
import java.util.*

/**
 * Compares two disk objects.
 *
 * Compares the specified tracks of two disk objects with optional verbose output.
 *
 * @param disk    the disk to compare against
 * @param start   the track on which to start the comparison
 * @param end     the track on which to end the comparison
 * @param logger  the logger to use for output
 * @param strings the localization dictionary
 */
fun Disk.compare(disk: Disk, start: Int = 0, end: Int = -1, logger: Logger, strings: ResourceBundle, ignoreInterleave: Boolean = false): Boolean {
    var result = true

    if (trackCount != disk.trackCount && start == 0 && end == -1) {
        logger.logn(LogLevel.ERROR, "!!${strings.getString("trackCountDifferent")} (${trackCount} vs. ${disk.trackCount})")
        result = false
    }

    val endTrack = if (end > -1) end else Math.min(trackCount, disk.trackCount) - 1

    for (tIdx in start..endTrack) {
        logger.logn(LogLevel.DETAIL, "${strings.getString("analyzingTrack")} $tIdx...")
        result = tracks[tIdx].compare(disk.tracks[tIdx], sectorsPerTrack, logger, strings, ignoreInterleave) && result
    }

    return result
}

/**
 * Compares two track objects.
 *
 * Compares the two track objects with optional verbose output.
 *
 * @param track   the track to compare against
 * @param logger  the logger to use for output
 * @param strings the localization dictionary
 */
fun Track.compare(track: Track, sectorsPerTrack: Int, logger: Logger, strings: ResourceBundle, ignoreInterleave: Boolean = false): Boolean {
    var result = true

    if (number != track.number) {
        logger.logn(LogLevel.ERROR, "!!${strings.getString("trackNumbersDifferent")}: $number / ${track.number}")
        result = false
    } else if (sectorSize != track.sectorSize) {
        logger.logn(LogLevel.ERROR, "!!${strings.getString("Track")} $number ${strings.getString("trackSectorSizeDifferent")}")
        result = false
    } else if (sectorCount != track.sectorCount) {
        logger.logn(LogLevel.ERROR, "!!${strings.getString("Track")} $number ${strings.getString("trackSectorCountDifferent")}")
        result = false
    } else {
        logger.logn(LogLevel.DETAIL, "  ${strings.getString("Track")} $number ${strings.getString("sectorSize")}: ${track.sectorSize}")
        logger.logn(LogLevel.DETAIL, "  ${strings.getString("Track")} $number ${strings.getString("sectorCount")}: $sectorCount")

        if (sectorCount > 0) {
            // determine if sector interleave is the same between the two tracks
            val sectorList = track.sectors.toMutableList()
            var matchedSectorInterleave = false
            var remainingRotates = track.sectorCount
            while (remainingRotates > 0) {
                if ((0 until track.sectorCount).all { sectors[it].number == sectorList[it].number }) {
                    matchedSectorInterleave = true
                    logger.logn(LogLevel.DETAIL, "  ${strings.getString("Track")} $number ${strings.getString("sectorInterleaveMatches")}")
                    break
                } else {
                    Collections.rotate(sectorList, 1)
                    remainingRotates--
                }
            }

            if (matchedSectorInterleave) {
                // print sector CRCs
                logger.log(LogLevel.CRC, "  ${strings.getString("Track")} ${String.format("%02d", track.number)} ${strings.getString("Sector")} CRCs\n  ")
                for (i in 0 until track.sectorCount) {
                    val src = sectors[i]
                    val tgt = sectorList[i]
                    logger.log(LogLevel.CRC, "  ${String.format("%02d", src.number)}=")
                    if (src.dataCrc.value == tgt.dataCrc.value) {
                        logger.log(LogLevel.CRC, String.format("%08X", src.dataCrc.value))
                    } else {
                        logger.log(LogLevel.CRC, "????????")
                    }
                    if ((i+1) % 6 == 0) {
                        logger.log(LogLevel.CRC, "\n  ")
                    }
                }
                if (track.sectorCount % 6 != 0) {
                    logger.newline(LogLevel.CRC)
                }
                logger.newline(LogLevel.CRC)

                // determine if sector data matches
                logger.logn(LogLevel.DUMP, "  ${strings.getString("Track")} ${String.format("%02d", track.number)} ${strings.getString("Data")} (${sectors.size} ${strings.getString("sectors")})")
                for (i in 0 until track.sectorCount) {
                    result = sectors[i].compare(number, sectorList[i], sectorsPerTrack, logger, strings) && result
                }
                logger.newline(LogLevel.DUMP)
            } else {
                logger.logn(LogLevel.ERROR, "!!${strings.getString("Track")} $number ${strings.getString("hasDifferentSectorInterleave")}")
                result = false
            }
        }
    }

    if (result) {
        logger.logn(LogLevel.DETAIL, "  ${strings.getString("trackDataMatches")}")
    }

    logger.newline(LogLevel.DETAIL)

    return result
}

/**
 * Compares two sector objects.
 *
 * @param trackNumber the track number the sectors belong to
 * @param sector      the sector to compare against
 * @param logger  the logger to use for output
 * @param strings the localization dictionary
 */
fun Sector.compare(trackNumber: Int, sector: Sector, sectorsPerTrack: Int, logger: Logger, strings: ResourceBundle): Boolean {
    var result = true

    if (crcError != sector.crcError) {
        logger.logn(LogLevel.ERROR, "!!${strings.getString("Track")} $trackNumber, ${strings.getString("Sector")} $number ${strings.getString("doesntHaveCrcMatch")}")
        result = false
    }
    if ((weakOffset != null && sector.weakOffset == null) || (weakOffset == null && sector.weakOffset != null)) {
        logger.logn(LogLevel.ERROR, "!!${strings.getString("Track")} $trackNumber, ${strings.getString("Sector")} $number ${strings.getString("doesntHaveMatchingWeakSegment")}")
        result = false
    }
    if (dataCrc.value != sector.dataCrc.value) {
        // weak sectors will likely always have CRC errors, so only report CRC problem if there's no weak data
        if (crcError == sector.crcError && crcError && weakOffset == null && sector.weakOffset == null) {
            logger.logn(LogLevel.ERROR, "!!${strings.getString("Track")} ${trackNumber}, ${strings.getString("Sector")} $number ${strings.getString("hasCrcMatchDifferentData")}")
            result = false
        // if the weak offsets are different, the data comparison will likely fail; report this situation
        } else if (weakOffset != sector.weakOffset) {
            logger.logn(LogLevel.INFO, "!!${strings.getString("Track")} ${trackNumber}, ${strings.getString("Sector")} $number ${strings.getString("hasDifferentWeakOffsets")}")
        } else {
            logger.logn(LogLevel.ERROR, "!!${strings.getString("Track")} $trackNumber, ${strings.getString("Sector")} $number (${getAbsoluteSectorForTrackAndSectorIndex(trackNumber, number, sectorsPerTrack)}) ${strings.getString("dataDoesntMatch")}: CRC[1]=${String.format("%08X", dataCrc.value)} CRC[2]=${String.format("%08X", sector.dataCrc.value)}")
            logger.logn(LogLevel.DUMP, "  1-S${String.format("%02d", sector.number)}: ${data?.toHexString() ?: strings.getString("noData")}")
            logger.logn(LogLevel.DUMP, "  2-S${String.format("%02d", sector.number)}: ${sector.data?.toHexString() ?: strings.getString("noData")}")
            result = false
        }
    } else {
        logger.logn(LogLevel.DUMP, "    S${String.format("%02d", sector.number)}: ${data?.toHexString() ?: strings.getString("noData")}")
    }

    return result
}