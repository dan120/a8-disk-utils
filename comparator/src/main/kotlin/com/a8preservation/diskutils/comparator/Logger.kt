/**
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
 */
package com.a8preservation.diskutils.comparator

enum class LogLevel {
    ERROR,
    INFO,
    DETAIL,
    CRC,
    DUMP,
    CSV
}

interface Logger {
    fun newline(level: LogLevel)
    fun logn(level: LogLevel, msg: String)
    fun log(level: LogLevel, msg: String)
}

class PrintLogger(val args: DiskCompareArgs): Logger {
    override fun newline(level: LogLevel) {
        log(level, "\n")
    }

    override fun logn(level: LogLevel, msg: String) {
        log(level, msg)
        newline(level)
    }

    override fun log(level: LogLevel, msg: String) {
        val result = when (level) {
            LogLevel.ERROR -> !args.csv
            LogLevel.INFO -> !args.csv
            LogLevel.DETAIL -> args.verbose
            LogLevel.CRC -> args.crc
            LogLevel.DUMP -> args.dump
            LogLevel.CSV -> args.csv
        }
        if (result) {
            print(msg)
        }
    }
}