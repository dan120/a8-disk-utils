/**
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
 */
package com.a8preservation.diskutils.comparator

import com.a8preservation.disklib.*
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.SystemExitException
import com.xenomachina.argparser.default
import java.io.File
import java.io.IOException
import java.util.*

class DiskCompareArgs(parser: ArgParser, strings: ResourceBundle) {
    val sfile by parser.positional(strings.getString("sFileDesc"))
    val dfile by parser.positional(strings.getString("dFileDesc"))
    val verbose by parser.flagging("-v", "--verbose", help = strings.getString("verboseDesc"))
    val dump by parser.flagging("-d", "--dump", help = strings.getString("dumpDesc"))
    val crc by parser.flagging("-c", "--crc", help = strings.getString("crcDesc"))
    val csv by parser.flagging("--csv", help = strings.getString("csvDesc"))
    val startTrack by parser.storing(strings.getString("startTrackDesc")).default("0")
    val endTrack by parser.storing(strings.getString("endTrackDesc")).default("-1")
}

/**
 * Read an Atari disk image.
 *
 * Reads the contents of an Atari 8-bit disk image file and returns a representative Disk object.
 *
 * @param file the disk image file
 */
fun readDiskImage(file: File): Disk {
    return when (file.extension.toLowerCase()) {
        "atx" -> readATX(file.inputStream())
        "atr" -> readATR(file.inputStream())
        else -> throw IOException("Unsupported file extension: ${file.extension}")
    }
}

/**
 * Compare two Atari 8-bit disk images.
 *
 * This function will compare the contents of two Atari 8-bit disk images with optional verbose output.
 *
 * @param file1      an image file
 * @param file2      an image file to compare file1 against
 * @param startTrack the track on which to start the comparison
 * @param endTrack   the track on which to end the comparison (or -1 for the whole disk)
 * @param logger  the logger to use for output
 * @param strings the localization dictionary
 */
fun compareDisks(file1: File, file2: File, startTrack: Int = 0, endTrack: Int = -1, logger: Logger, strings: ResourceBundle): Boolean {
    // make sure the files exist
    if (!file1.isFile)
        throw RuntimeException("${file1.absolutePath} ${strings.getString("notFoundSuffix")}")
    if (!file2.isFile)
        throw RuntimeException("${file2.absolutePath} ${strings.getString("notFoundSuffix")}")

    // read the disk images and perform comparison
    return readDiskImage(file1).compare(readDiskImage(file2), startTrack, endTrack, logger, strings)
}

fun printDiskCompareHeader(props: Properties, strings: ResourceBundle) {
    println("${strings.getString("programName")} v${props.get("version")}")
    println(strings.getString("programCopyright"))
    println(strings.getString("programLicense"))
    println(strings.getString("programHint"))
    println()
}

fun main(cargs : Array<String>) {
    val strings = ResourceBundle.getBundle("strings")

    val props = Properties()
    props.load(ClassLoader.getSystemResource("version.properties").openStream())

    try {
        val args = DiskCompareArgs(ArgParser(cargs), strings)
        val logger = PrintLogger(args)

        if (!args.csv) {
            printDiskCompareHeader(props, strings)
        }

        val file1 = File(args.sfile)
        val file2 = File(args.dfile)

        logger.log(LogLevel.INFO, "${strings.getString("Comparing")}: ${file1.name}[1] ${strings.getString("and")} ${file2.name}[2]\n")
        logger.log(LogLevel.CSV, "\"${file1.absolutePath}\",\"${file2.absolutePath}\",")

        if (compareDisks(file1, file2, args.startTrack.toInt(), args.endTrack.toInt(), logger, strings)) {
            logger.newline(LogLevel.INFO)
            logger.logn(LogLevel.INFO, strings.getString("comparisonSucceeded"))
            logger.log(LogLevel.CSV, "true")
            System.exit(0)
        } else {
            logger.newline(LogLevel.INFO)
            logger.logn(LogLevel.INFO, strings.getString("comparisonFailed"))
            logger.log(LogLevel.CSV, "false")
            System.exit(1)
        }
    } catch (e: SystemExitException) {
        printDiskCompareHeader(props, strings)
        e.printAndExit("a8DiskCompare", 80)
    }
}
